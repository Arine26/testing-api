import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient ,HttpParams} from '@angular/common/http';
import { Posts } from '../classes/post';

@Injectable()
export class Api{
    constructor(private httpclient:HttpClient){

    }

    getcomments():Observable<any>{
        return this.httpclient.get("http://dummy.restapiexample.com/api/v1/employees")
    }

    getPost(postData:Posts):Observable<any>{
        return this.httpclient.post("http://dummy.restapiexample.com/api/v1/create",postData);
    }

    update(updateData:Posts,id:string):Observable<any>{
        return this.httpclient.put("http://dummy.restapiexample.com/api/v1/update/"+id,updateData)
    }

   

}