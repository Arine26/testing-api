import { Component } from '@angular/core';
import {Api} from './services/Api.compnent';
import { Comments } from './classes/comments';
import { Getparameter } from './classes/getparameter';
import { Posts } from './classes/post';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isOpenSignUp:boolean;
  id:string;
  constructor(private _Api:Api, private router:Router){
  }
  lstcomments:Comments[];
  lstcommentsCopy:Comments[];
  lstPosts:Getparameter;
  oposts:Posts;
  editData:Comments;

  ngOnInit(): void { 
    
   this._Api.getcomments()
   .subscribe(
     data=>
     {
       this.lstcomments=data;
       this.lstcommentsCopy = this.lstcomments; 
     }

   );

  //  this._Api.getcommnetsbtparameter()
  //  .subscribe
  //  (
  //    data=>
  //    {
  //       this.lstPosts =data;
  //    }
  //  );  
   
   this.oposts = new Posts();
    }

  onClickSubmit(){
    console.log(this.oposts);
    this._Api.getPost(this.oposts)
    .subscribe(
      data =>{
        console.log(data);
      }
    );
    }

    openComponent(){
      this.isOpenSignUp =true;
    }
    closeComponent(){
      this.isOpenSignUp=false
    }

    openEditComponent(data: Comments){
      this.isOpenSignUp=true;
      this.editData = data;
    }

    onClickSearch(){

      this.lstcomments=[];
      console.log(this.lstcomments);
      console.log(this.lstcommentsCopy);
      
      for(let i=0;i<this.lstcommentsCopy.length;i++){
        if(this.id == this.lstcommentsCopy[i].id){
          this.lstcomments.push(this.lstcommentsCopy[i]);
        }
      }
    }
    
    
}
