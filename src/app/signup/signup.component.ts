import { Component, OnInit, Input } from '@angular/core';
import { Posts } from '../classes/post';
import { Api } from '../services/Api.compnent';
import { Comments } from '../classes/comments';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  isOpenSignUp:boolean;
  @Input() commentData:Comments;
  id:string;
  
  oposts:Posts;
  constructor(private _Api:Api,) { }

  ngOnInit() {
    this.oposts = new Posts();
    if(this.commentData!= null || this.commentData != undefined){
      this.oposts.name = this.commentData.employee_name;
      this.oposts.age = this.commentData.employee_age;
      this.oposts.salary = this.commentData.employee_salary;
      this.id = this.commentData.id;
    }
  }
  onClickSubmit(){
    console.log(this.oposts);
    if(this.commentData == null || this.commentData == undefined){
      this._Api.getPost(this.oposts)
      .subscribe(
        data =>{
          console.log(data);
        }
      );
    }else{
      this._Api.update(this.oposts,this.id)
      .subscribe(
        data=>
        {

        }
      )
    }
    
    }
   
}
